ARG DOCKER_REPO
FROM ${DOCKER_REPO}/node:18

WORKDIR /app

RUN npm i -g typescript

COPY package*.json ./

RUN npm i

COPY . .

RUN chmod uag+r token.json
RUN chmod uag+r credentials.json

RUN tsc

CMD ["node", "/app/out/src/index.js"]