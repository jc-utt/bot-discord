import { SlashCommandBuilder, ChatInputCommandInteraction } from 'discord.js'
import { updateValue, getValue } from "../dbutils"
import { getsheetName } from '../gSheets';

module.exports = {
	data: new SlashCommandBuilder()
		.setName('change-sheet')
		.setDescription('change l\'ID du GSheet')
		.addStringOption(option =>
			option.setName('sheet-id')
				.setDescription('l\'ID du GSheet (https://docs.google.com/spreadsheets/d/<ID DU GSHEET>/edit...)')
				.setRequired(false)),
	async execute(interaction: ChatInputCommandInteraction) {
		setTimeout(async () => {
			if(!interaction.replied)
				await interaction.deferReply()
		}, 2000)//au cas où la réponse est trop longue
		try {
			if (interaction.options.getString("sheet-id"))
				await updateValue("sheetID", interaction.options.getString("sheet-id") as string)
			let sheetID = await getValue("sheetID")
			let sheetName = await getsheetName()
			let res = `Done.\nsheet id = ${sheetID || "NOT SET"}\nsheet name (onglet) = ${sheetName || "NOT SET"}`
			if(interaction.deferred)
				await interaction.editReply(res)
			else
				await interaction.reply(res)
		}
		catch (err:any) {
			if(interaction.deferred)
				await interaction.editReply(err.msg || err).catch(console.error)
			else
				await interaction.reply(err.msg||err).catch(console.error)
		}
	},
};